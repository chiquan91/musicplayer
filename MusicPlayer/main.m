//
//  main.m
//  MusicPlayer
//
//  Created by Hoang Chi Quan on 10/18/15.
//  Copyright © 2015 Hoang Chi Quan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
