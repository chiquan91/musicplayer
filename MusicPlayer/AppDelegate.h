//
//  AppDelegate.h
//  MusicPlayer
//
//  Created by Hoang Chi Quan on 10/18/15.
//  Copyright © 2015 Hoang Chi Quan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

