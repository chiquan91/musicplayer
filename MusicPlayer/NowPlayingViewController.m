//
//  NowPlayingViewController.m
//  MusicPlayer
//
//  Created by Hoang Chi Quan on 10/18/15.
//  Copyright © 2015 Hoang Chi Quan. All rights reserved.
//

#import "NowPlayingViewController.h"
#import "EFCircularSlider.h"

@interface NowPlayingViewController ()
@property (weak, nonatomic) IBOutlet UIButton *songImageCover;
@property (weak, nonatomic) IBOutlet UIButton *topButton;

@property (weak, nonatomic) IBOutlet UILabel *songName;
@property (weak, nonatomic) IBOutlet UILabel *songAuthor;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

@property (nonatomic, strong) EFCircularSlider *slider;

@property (weak, nonatomic) IBOutlet MPVolumeView *volumeView;
@property (nonatomic, strong) MPMusicPlayerController *musicPlayer;

@property (weak, nonatomic) IBOutlet UILabel *totalTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;

@end

@implementation NowPlayingViewController

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
// Setup navigation bar
    _musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    [self registerMediaPlayerNotifications];
    
// Setup imageView
    _songImageCover.layer.cornerRadius = self.songImageCover.frame.size.width / 2;
    _songImageCover.layer.masksToBounds = YES;

// Setup imageCover, song's name, artist
    [self setupViews];
    
//    [self.volumeView setMinimumVolumeSliderImage:[UIImage imageNamed:@"volumedown"] forState:UIControlStateNormal];
//    [self.volumeView setMaximumVolumeSliderImage:[UIImage imageNamed:@"volumeup"] forState:UIControlStateNormal];
    

    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(updateProgress)
                                   userInfo:nil
                                    repeats:YES];
    
    CGRect coverFrame = self.songImageCover.frame;
    CGFloat distant = 5.0f;
    _slider = [[EFCircularSlider alloc] initWithFrame:CGRectMake(coverFrame.origin.x - distant, coverFrame.origin.y - distant, coverFrame.size.width + distant * 2, coverFrame.size.height + distant * 2)];
    _slider.minimumValue = 0.0f;
    _slider.maximumValue = 1.0f;
    [_slider addTarget:self action:@selector(changePlayback:) forControlEvents:UIControlEventValueChanged];

    [self.view addSubview:_slider];
    [self.view bringSubviewToFront:_topButton];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_musicPlayer.playbackState == MPMusicPlaybackStatePlaying) {
        _btnPlay.selected = NO;
        _songImageCover.selected = NO;
        _topButton.selected = NO;
        [self startNewAnimation];
    }
}

- (void)dealloc {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self
                                  name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                object:_musicPlayer];
    [notificationCenter removeObserver:self
                                  name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                object:_musicPlayer];
    [notificationCenter removeObserver:self
                                  name:MPMusicPlayerControllerVolumeDidChangeNotification
                                object:_musicPlayer];
    [_musicPlayer endGeneratingPlaybackNotifications];
}

#pragma mark IBActions

- (IBAction)btnPreviousTapped:(id)sender {
    if (_musicPlayer.indexOfNowPlayingItem == 0) {
        [_musicPlayer stop];
        [_musicPlayer play];
    } else {
        [_musicPlayer skipToPreviousItem];
    }
}

- (IBAction)btnPlayTapped:(id)sender {
    UIButton *playButton = sender;
    if (playButton.isSelected == YES) {
        [_musicPlayer play];
        _topButton.selected = NO;
        _btnPlay.selected = NO;
        _songImageCover.selected = NO;
        [self resumeLayer:_songImageCover.layer];
    } else {
        [_musicPlayer pause];
        _topButton.selected = YES;
        _btnPlay.selected = YES;
        _songImageCover.selected = YES;
        [self pauseLayer:_songImageCover.layer];
    }
}

- (IBAction)btnNextTapped:(id)sender {
    
    if (_musicPlayer.indexOfNowPlayingItem == [[MPMediaQuery songsQuery].items count] - 1) {
        [_musicPlayer stop];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [_musicPlayer skipToNextItem];
    }
}

- (IBAction)btnRepeatTapped:(id)sender {
    UIButton *btnRepeat = sender;
    if ([btnRepeat.titleLabel.text isEqualToString:@"Repeat Off"]) {
        [btnRepeat setTitle:@"Repeat One" forState:UIControlStateNormal];
        _musicPlayer.repeatMode = MPMusicRepeatModeOne;
    } else if ([btnRepeat.titleLabel.text isEqualToString:@"Repeat One"]){
        [btnRepeat setTitle:@"Repeat All" forState:UIControlStateNormal];
        _musicPlayer.repeatMode = MPMusicRepeatModeAll;
    } else if ([btnRepeat.titleLabel.text isEqualToString:@"Repeat All"]) {
        [btnRepeat setTitle:@"Repeat Off" forState:UIControlStateNormal];
        _musicPlayer.repeatMode = MPMusicRepeatModeNone;
    }
}

- (IBAction)btnShuffleTapped:(id)sender {
    UIButton *btnShuffle = sender;
    if ([btnShuffle.titleLabel.text isEqualToString:@"Shuffle Off"]) {
        [btnShuffle setTitle:@"Shuffle On" forState:UIControlStateNormal];
        _musicPlayer.shuffleMode = MPMusicShuffleModeSongs;
    } else if ([btnShuffle.titleLabel.text isEqualToString:@"Shuffle On"]) {
        [btnShuffle setTitle:@"Shuffle Off" forState:UIControlStateNormal];
        _musicPlayer.shuffleMode = MPMusicShuffleModeOff;
    }
}

#pragma mark - Privates

- (void)registerMediaPlayerNotifications {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                           selector:@selector(nowPlayingItemChange:)
                               name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                             object:_musicPlayer];
    
    [notificationCenter addObserver:self
                           selector:@selector(playbackStateChange:)
                               name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                             object:_musicPlayer];
    
    [notificationCenter addObserver:self
                           selector:@selector(volumeChange:)
                               name:MPMusicPlayerControllerVolumeDidChangeNotification
                             object:_musicPlayer];
    [_musicPlayer beginGeneratingPlaybackNotifications];
}

- (void)nowPlayingItemChange:(id)notification {
    [self setupViews];
    [self startNewAnimation];
}

- (void)playbackStateChange:(id)notification {
}

- (void)volumeChange:(id)notification {
}

- (void)setupViews {
    MPMediaItem *item = [_musicPlayer nowPlayingItem];
    
    MPMediaItemArtwork *artwork = item.artwork;
    UIImage *imageCover = [artwork imageWithSize:CGSizeMake(240, 240)];
    if (imageCover) {
        [self.songImageCover setBackgroundImage:imageCover forState:UIControlStateNormal];
    } else {
        [self.songImageCover setBackgroundImage:[UIImage imageNamed:@"nowPlayingImage"] forState:UIControlStateNormal];
    }
    
    self.songName.text = item.title;
    self.songAuthor.text = [NSString stringWithFormat:@"%@ - %@ - %@", item.artist, item.composer, item.albumTitle];
    
    // Setup timeStemp
    
    NSInteger totalTime = (NSInteger)_musicPlayer.nowPlayingItem.playbackDuration;
    self.totalTimeLabel.text = [NSString stringWithFormat:@"%2.2ld : %2.2ld", (long)totalTime / 60, (long)totalTime % 60];
    
    NSInteger currentTime = (NSInteger)_musicPlayer.currentPlaybackTime;
    self.currentTimeLabel.text = [NSString stringWithFormat:@"%2.2ld : %2.2ld", (long)currentTime / 60, (long)currentTime % 60];
}

- (void)updateProgress {
    _slider.currentValue = _musicPlayer.currentPlaybackTime / _musicPlayer.nowPlayingItem.playbackDuration;
    NSInteger currentTime = (NSInteger)_musicPlayer.currentPlaybackTime;
    self.currentTimeLabel.text = [NSString stringWithFormat:@"%2.2ld : %2.2ld", (long)currentTime / 60, (long)currentTime % 60];
}

- (void)changePlayback:(id)sender {
    [_musicPlayer setCurrentPlaybackTime:_slider.currentValue * _musicPlayer.nowPlayingItem.playbackDuration];
}

- (void)pauseLayer:(CALayer *)layer {
    CFTimeInterval pausedTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil];
    layer.speed = 0.0;
    layer.timeOffset = pausedTime;
}

- (void)resumeLayer:(CALayer *)layer {
    CFTimeInterval pausedTime = [layer timeOffset];
    layer.speed = 1.0;
    layer.timeOffset = 0.0;
    layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    layer.beginTime = timeSincePause;
}

- (void)startNewAnimation {
    [_songImageCover.layer removeAllAnimations];
    
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0];
    rotationAnimation.duration = 50.0f;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 10;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [_songImageCover.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];

}


@end
