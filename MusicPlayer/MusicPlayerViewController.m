//
//  MusicPlayerViewController.m
//  MusicPlayer
//
//  Created by Hoang Chi Quan on 10/18/15.
//  Copyright © 2015 Hoang Chi Quan. All rights reserved.
//

#import "MusicPlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "NowPlayingViewController.h"

@interface MusicPlayerViewController () <UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic, strong) NSMutableArray *songFilters;

@end

@implementation MusicPlayerViewController

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Songs";
    
    self.songFilters = [NSMutableArray array];
}

#pragma mark - Prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    MPMediaQuery *query = [MPMediaQuery songsQuery];
    NSArray *songs = [query items];
    if ([tableView isEqual:self.tableView]) {
        if ([songs count] == 0) {
            return 5;
        } else {
            return [songs count];
        }
    } else {
        return [self.songFilters count];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SongCell" forIndexPath:indexPath];
    MPMediaQuery *query = [MPMediaQuery songsQuery];
    NSArray *songs;

    if ([tableView isEqual:self.tableView]) {
        songs = query.items;
    } else {
        songs = self.songFilters;
    }

    if ([songs count] == 0) {
        cell.imageView.image = [UIImage imageNamed:@"songImage"];
        cell.textLabel.text = @"Song's name";
        cell.detailTextLabel.text = @"Song's infos";
        return cell;
    }
    
    MPMediaItem *item = songs[indexPath.row];
    // Get song image
    MPMediaItemArtwork *artwork = item.artwork;
    UIImage *songImage = [artwork imageWithSize:CGSizeMake(44, 44)];
    if (songImage) {
        cell.imageView.image = songImage;
    } else {
        cell.imageView.image = [UIImage imageNamed:@"songImage"];
    }
    
    // Get song title
    cell.textLabel.text = item.title;
    
    // Get song infos
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@", item.artist, item.composer, item.albumTitle];
    
    return cell;
}

#pragma mark - Table view delegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index;
    MPMediaQuery *query = [MPMediaQuery songsQuery];
    NSArray *songs;
    
    if ([tableView isEqual:self.tableView]) {
        
        index = [self.tableView indexPathForSelectedRow].row;
        songs = query.items;
        
    } else {
        index = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow].row;
        songs = self.songFilters;
    }
    
    if ([songs count] != 0) {
        MPMediaItem *item = [[songs objectAtIndex:index] representativeItem];
        
        MPMusicPlayerController *musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
        [musicPlayer setQueueWithItemCollection:[MPMediaItemCollection collectionWithItems:query.items]];
        [musicPlayer setNowPlayingItem:item];
        
        [musicPlayer play];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
}

#pragma mark - UISearchDisplayDelegate

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView {
//
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterContentForSearchText:searchString scope:[self.searchDisplayController.searchBar scopeButtonTitles][self.searchDisplayController.searchBar.selectedScopeButtonIndex]];
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[self.searchDisplayController.searchBar scopeButtonTitles][searchOption]];
    return YES;
}

#pragma mark Privates

- (void)filterContentForSearchText:(NSString *)text scope:(NSString *)scope {
    [self.songFilters removeAllObjects];
    
    NSPredicate *preficate = [NSPredicate predicateWithFormat:@"self.title contains[c] %@", text];
    
    NSArray *songs = [MPMediaQuery songsQuery].items;
    
    self.songFilters = [NSMutableArray arrayWithArray:[songs filteredArrayUsingPredicate:preficate]];
}
@end
